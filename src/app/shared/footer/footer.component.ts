import { Component } from '@angular/core';
import { footerData } from './footer-data';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
    ingredients = footerData.ingredients;
    section1 = footerData.section1;
    section2 = footerData.section2;
    section3 = footerData.section3;
    section4 = footerData.section4;
    section5 = footerData.section5;
    section6 = footerData.section6;

    constructor() {}
}
