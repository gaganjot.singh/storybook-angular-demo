import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgSelectModule } from "@ng-select/ng-select";
import {
  CloudinaryModule,
  CloudinaryConfiguration
} from "@cloudinary/angular-5.x";
import { Cloudinary } from "cloudinary-core/cloudinary-core-shrinkwrap";
import { DropdownComponent } from "./dropdown/dropdown.component";
import { FlickityCarousel } from "./flickity-carousel/carousel.component";
import { FooterComponent } from "./footer/footer.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ImageComponent } from "./image/image.component";
import { SvgComponent } from "./svg/svg.component";
import { IngredientBarComponent } from "./ingredient-bar/ingredient-bar.component";
import { H5Component } from "./headers/h5.component";
import { ClickableComponent } from "./clickables/clickable.component";
import { CopyComponent } from "./text/copy.component";
import { CaptionComponent } from "./text/caption.component";
import { SortByDropdownComponent } from "./sort-by-dropdown/sort-by-dropdown.component";

const COMPONENTS = [
  DropdownComponent,
  FlickityCarousel,
  FooterComponent,
  ImageComponent,
  SvgComponent,
  IngredientBarComponent,
  H5Component,
  ClickableComponent,
  CopyComponent,
  CaptionComponent,
  SortByDropdownComponent
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    CommonModule,
    NgSelectModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
    CloudinaryModule.forRoot({ Cloudinary }, {
      cloud_name: "idemo",
      secure: true
    } as CloudinaryConfiguration)
  ],
  exports: [COMPONENTS]
})
export class SharedModule {}
