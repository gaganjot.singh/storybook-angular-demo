import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-svg',
    templateUrl: './svg.component.html',
    styleUrls: ['./svg.component.scss'],
})
export class SvgComponent implements OnInit {
    @Input() svgStyles: string;
    @Input() aria: string;
    @Input() title: string;
    @Input() desc: string;
    @Input() width: string;
    @Input() height: string;
    @Input() svgId: string;
    ariaTitle: string;
    ariaDesc: string;
    styles: string;

    ngOnInit() {
        this.ariaTitle = 'title-' + this.aria;
        this.ariaDesc = 'desc-' + this.aria;
        this.styles = 'svg ' + this.svgStyles;
    }

    constructor() {}
}
