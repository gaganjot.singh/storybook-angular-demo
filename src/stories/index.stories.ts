import { storiesOf, moduleMetadata } from "@storybook/angular";
import { NgSelectModule } from "@ng-select/ng-select";
import { SharedModule } from "../app/shared/shared.module";

storiesOf("Shared Components/Custom Dropdown", module)
  .addDecorator(
    moduleMetadata({
      imports: [SharedModule, NgSelectModule]
    })
  )
  .add("Footer", () => {
    return {
      template: `
                <app-footer></app-footer>
            `
    };
  })
  .add("Product Grid Filters", () => ({
    props: {
      productTypeTitle: "Product Type",
      productTypeItems: [
        { label: "Cleansers" },
        { label: "Treatments" },
        { label: "Stylers" },
        { label: "Tools" },
        { label: "Kits" },
        { label: "Gifts" }
      ],
      curlTypeTitle: "Curl Type",
      curlTypeItems: [
        {
          image: "dev/deva-before-sample-dev",
          label: "Wavy"
        },
        {
          image: "dev/deva-before-sample-dev",
          label: "Curly"
        },
        {
          image: "dev/deva-before-sample-dev",
          label: "Super Curly"
        }
      ],
      benefitTitle: "Benefits",
      benefitItems: [
        { label: "Avocode" },
        { label: "Didn't" },
        { label: "List" },
        { label: "These" }
      ],
      sortByTitle: "Sort By",
      sortByItems: [
        { label: "Best Sellers" },
        { label: "A-Z" },
        { label: "Trending" }
      ]
    },
    template: `
        <div class="flex-row margin-t-30">
            <app-dropdown
                class="inline-block filter-width"
                [title]="productTypeTitle"
                [items]="productTypeItems"
                [multiple]="true"
            >
            </app-dropdown>
            <app-dropdown
                class="inline-block filter-width"
                [title]="curlTypeTitle"
                [items]="curlTypeItems"
                [multiple]="true"
            >
            </app-dropdown>
            <app-dropdown
                class="inline-block filter-width"
                [title]="benefitTitle"
                [items]="benefitItems"
                [multiple]="true"
            >
            </app-dropdown>
            <app-dropdown
                class="inline-block sort-by-width"
                [title]="sortByTitle"
                [items]="sortByItems"
                [multiple]="false"
            >
            </app-dropdown>
        </div>
        <div class="margin-t-30">
            <app-clickable theme="text-link" display="inline-block" href="https://stackblitz.com/edit/ng-select?file=app%2Fapp.component.ts">Here's a helpful reference
            </app-clickable>&nbsp;for using ng-select
        </div>
        `
  }))
  .add("Sort By Dropdown", () => ({
    props: {
      sortByTitle: "Sort By",
      sortByItems: [
        { label: "Best Sellers" },
        { label: "Newest" },
        { label: "Top Rated" },
        { label: "Price Low-High" },
        { label: "Price High-Low" }
      ]
    },
    template: `
        <div class="sort-by-width">
            <app-sort-by-dropdown
                [title]="sortByTitle"
                [items]="sortByItems"
            ></app-sort-by-dropdown>
        </div>

        `
  }));
